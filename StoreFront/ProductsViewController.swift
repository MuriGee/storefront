//
//  ProductsViewController.swift
//  StoreFront
//
//  Created by Muri Gumbodete on 08/07/2019.
//  Copyright © 2019 MuriGee. All rights reserved.
//

import UIKit
import Firebase

class ProductsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func SingOutButtonTapped(_ sender: Any) {
        try! Auth.auth().signOut()
        self.performSegue(withIdentifier: "signOut", sender: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
