//
//  ViewController.swift
//  StoreFront
//
//  Created by Muri Gumbodete on 27/06/2019.
//  Copyright © 2019 MuriGee. All rights reserved.
//

import UIKit
import Firebase

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var FNameTextField: UITextField!
    @IBOutlet weak var LNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    let db = Firestore.firestore().collection("users")
    var ref: DocumentReference? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    func checkFields() {
        if FNameTextField.text!.isEmpty || LNameTextField.text!.isEmpty || emailTextField.text!.isEmpty || passwordTextField.text!.isEmpty || confirmPasswordTextField.text!.isEmpty {
            let alert = UIAlertController(title: "Uncomplete Form", message: "Please make sure all fields have been filled in." , preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            
            self.present(alert, animated: true, completion: nil)
            
            print("Failed")
        } else {
            checkPasswordValidity()
        }
    }
    
    func checkPasswordValidity() {
        if passwordTextField.text! != confirmPasswordTextField.text! {
            let alert = UIAlertController(title: "Re-Enter Passwords", message: "Passwords entered are not the same. Please re-enter" , preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            
            self.present(alert, animated: true, completion: nil)
            
            print("Failed Password")
            passwordTextField.text = ""
            confirmPasswordTextField.text = ""
        } else {
            handleRegistration()
        }
    }
    
    func handleRegistration() {
        Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { user, error in
            if error != nil {
                let alert = UIAlertController(title: "Registration Failed",
                                              message: error?.localizedDescription,
                                              preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default))
                
                self.present(alert, animated: true, completion: nil)
            } else {
                guard let uid = user?.user.uid else { return }
                
                let usersName = self.FNameTextField.text! + " " + self.LNameTextField.text!
                
                let userValues = ["email": self.emailTextField.text!, "name": usersName]
                
                self.db.document(uid).setData(userValues)
                
                Auth.auth().signIn(withEmail: self.emailTextField.text!, password: self.passwordTextField.text!)
                
                self.performSegue(withIdentifier: "loginComplete", sender: self)
                
                print("Success")
            }
        }
    }
    
    @IBAction func registerButtonTouched(_ sender: Any) {
        checkFields()
    }
}
